//
//  MapView.swift
//  menupageui
//
//  Created by Vishal Jain on 13/07/22.
//

import SwiftUI
import MapKit

struct MapView: View {
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 34.011_286, longitude: -116.166_868),
        span: MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
    )

    var body: some View {
        VStack {
            Map(coordinateRegion: $region)
            
        }
        .frame(width: .infinity, height: 350)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
        
    }
}
