//
//  Actionsheet.swift
//  menupageui
//
//  Created by Vishal Jain on 25/07/22.
//

import SwiftUI
struct Actionsheet: View {
    var body: some View {
        HomeOne()
    }
}

struct Actionsheet_Previews: PreviewProvider {
    static var previews: some View {
        Actionsheet()
    }
}

struct HomeOne  : View{
    
    @State var edges = UIApplication.shared.windows.first?.safeAreaInsets
    
    @State var showFilter = false
    
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .trailing, vertical: .top), content:{

            
            Button (action: {
                withAnimation{
                    showFilter.toggle()
                    
                }
            }, label:{
                Image("on (1)")
                    .font(.title2)
                    .foregroundColor(.black)
                    .padding(.vertical,10)
                    .padding(.horizontal,15)
                    .background(Color.green.opacity(0.01))
                    .cornerRadius(10)
                    .shadow(color: .black.opacity(0.1), radius: 5, x: -5, y: -5)
            })
                .padding(.trailing,3)
                .padding(.top,5)
            
            // Filters and Radio buttons
            
            VStack{
                
                Spacer()
                
                VStack( spacing: 25){
                    
                    HStack {
                        Text("Settings")
                            .font(.title)
                            .fontWeight(.semibold)
                            .foregroundColor(.black)
                        
                        Spacer()
                        
                        Button(action: {
                            withAnimation{
                                showFilter.toggle()
                            }
                        }) {
                            Image("X (1)")
                                .resizable()
                                .frame(width: 15, height: 15)
                        }
                        
                    }
                    .padding([.horizontal,.top])
                    .padding(.bottom,10)
                    .padding(.horizontal,32)
                    
                    ForEach(filters){ filters in
                       CardView(filters: filters)
                    }
                    
                }
                .padding(.bottom,10)
                .padding(.bottom,edges?.bottom)
                .padding(.top)
                .background(Color.white.clipShape(customCorner(corners: [.topLeft,.topRight])))
                .offset( y: showFilter ? 0 : UIScreen.main.bounds.height / 2)
            }
            .ignoresSafeArea()
            .background(
                Color.black.opacity(0.3).ignoresSafeArea()
                    .opacity(showFilter ? 1 : 0)
            )
        })
    }
}

struct customCorner : Shape {
    
    var corners : UIRectCorner
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners,cornerRadii: CGSize(width: 35, height: 35))
        return Path(path.cgPath)
    }
}

// cardvView ...

struct CardView : View {
    @State var filters : FilterItem
    var body: some View {
        
        HStack( spacing: 20){
            Image(filters.photo)
                .resizable()
                .frame(width: 35, height: 35)
            Text(filters.title)
                .fontWeight(.semibold)
                .foregroundColor(Color.black.opacity(0.7))
            
            Spacer()
            
        }
        .padding(.horizontal,30)
        .contentShape(Rectangle())
    }
}



struct FilterItem :  Identifiable {
    
    var id = UUID().uuidString
    var photo : String
    var title : String
//    var checked : Bool
}

var filters = [
    FilterItem(photo: "restart_service", title: "Restart Service"),
    FilterItem(photo: "sync_data", title: "Sync data"),
    FilterItem(photo: "faq", title: "FAQ"),
    FilterItem(photo: "chat", title: "Chat"),
    FilterItem(photo: "term_condition", title: "Terms and condition"),
    FilterItem(photo: "device_info", title: "Device info")
]
