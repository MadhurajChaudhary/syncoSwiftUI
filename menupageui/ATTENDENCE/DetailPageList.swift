//
//  DetailPageList.swift
//  menupageui
//
//  Created by Vishal Jain on 20/07/22.
//

import SwiftUI

struct DetailPageList: View {
    struct Photo: Decodable, Hashable {
        var image: String
        var title:String
        var desc :String
    }

    var menuIcons = [
        Photo(image: "green", title: "Present", desc: "0"),
        Photo(image: "grey", title: "Absent", desc: "0"),
        Photo(image: "red", title: "Leave", desc: "0"),
        Photo(image: "sky_blue", title: "Holiays", desc: "0"),
        Photo(image: "single_order", title: "Single order", desc: "View ongoing Singal Orders"),
        Photo(image: "multi_order", title: "Multi Order", desc: "View ongoing Singal Orders"),
        Photo(image: "order_history", title: "Order History", desc: "View Order History"),
        Photo(image: "scheduler", title: " Route scheduler", desc: "View Schedule Route"),
        Photo(image: "cash", title: "Deposit Cash", desc: "View Schedule Route"),
        Photo(image: "live", title: "Live Stream", desc: "View Schedule Routeh"),
        Photo(image: "distance", title: "TraveL Distance", desc: "View Schedule Routeh"),
        Photo(image: "geotag", title: "Geoag", desc: "View Schedule Routeh")
    ]
    
    var body: some View {
        ScrollView{
            
            VStack(alignment: .leading, spacing: 1){
                    HStack( spacing: 20){
                        Image("green").resizable()
                            .frame(width: 20, height: 20 )
                            
                        VStack(alignment: .leading, spacing: 2){
                            Text("Present") .frame(width: 100, height: 20, alignment: .leading)
                                .font(.title2)

                        }
                        Spacer()
                        
                        VStack (alignment: .leading){
                            Text("0")
                                .fontWeight(.bold)
                                .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))

                        }
                        Spacer()
                        
                        //buttons
                        
                        Button(action: {
                            print("button pressed")
                        }) {
                            Image("arrow")
                                .resizable()
                                .frame(width: 20, height: 20)
                        }
                    }.padding(.all)
                
                
                
                HStack( spacing: 20){
                    Image("grey").resizable()
                        .frame(width: 22, height: 22 )
                        
                    VStack(alignment: .leading, spacing: 2){
                        Text("Absent") .frame(width: 100, height: 20, alignment: .leading)
                            .font(.title2)

                    }
                    Spacer()
                    
                    VStack (alignment: .leading){
                        Text("0")
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))
                    }
                    Spacer()
                    
                    
                    //buttons
                    Button(action: {
                        print("button pressed")
                    }) {
                        Image("arrow")
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                }.padding(.all)
                
                
                
                HStack( spacing: 20){
                    Image("red").resizable()
                        .frame(width: 20, height: 20 )
                        
                    VStack(alignment: .leading, spacing: 2){
                        Text("Leave").frame(width: 100, height: 20, alignment: .leading)
                            .font(.title2)

                    }
                    Spacer()
                    
                    VStack {
                        Text("0")
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))

                    }
                    Spacer()
                    
                    //buttons
                    Button(action: {
                        print("button pressed")
                    }) {
                        Image("arrow")
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                }.padding(.all)
                
                HStack( spacing: 20){
                    Image("sky_blue").resizable()
                        .frame(width: 20, height: 20 )
                        
                    VStack(alignment: .leading, spacing: 2){
                        Text("Holidays").frame(width: 100, height: 20, alignment: .leading)
                            .font(.title2)

                    }
                    Spacer()
                    
                    VStack (alignment: .leading){
                        Text("3")
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))

                    }
                    
                    Spacer()
                    
                    //buttons
                    
                    Button(action: {
                        print("button pressed")
                    }) {
                        Image("arrow")
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                }.padding(.all)
                
                HStack( spacing: 20){
                    Image("pink").resizable()
                        .frame(width: 20, height: 20 )
                        
                    VStack(alignment: .leading, spacing: 2){
                        Text("Half Days").frame(width: 100, height: 20, alignment: .leading)
                            .font(.title2)

                    }
                    Spacer()
                    
                    VStack (alignment: .leading){
                        Text("0")
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))

                    }
                    Spacer()
                    
                    //buttons
                    
                    Button(action: {
                        print("button pressed")
                    }) {
                        Image("arrow")
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                }.padding(.all)
                
                
                HStack( spacing: 20){
                    Image("yellow").resizable()
                        .frame(width: 20, height: 20 )
                        
                    VStack(alignment: .leading, spacing: 2){
                        Text("Late Mark") .frame(width: 100, height: 20, alignment: .leading)
                            .font(.title2)

                    }
                    Spacer()
                    
                    VStack (alignment: .leading){
                        Text("0")
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))

                    }
                    Spacer()
                    
                    //buttons
                    
                    Button(action: {
                        print("button pressed")
                    }) {
                        Image("arrow")
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                }.padding(.all)
                
                HStack( spacing: 20){
                    Image("blue").resizable()
                        .frame(width: 20, height: 20 )
                        
                    VStack(alignment: .leading, spacing: 2){
                        Text("weekend").frame(width: 100, height: 20, alignment: .leading)
                            .font(.title2)

                    }
                    Spacer()
                    
                    VStack (alignment: .leading){
                        Text("30")
                            .fontWeight(.bold)
                            .foregroundColor(Color(red: 54/255, green: 201/255, blue: 181/255))
                    }
                    Spacer()
                    
                    //buttons
                    
                    Button(action: {
                        print("button pressed")
                    }) {
                        Image("arrow")
                            .resizable()
                            .frame(width: 20, height: 20)
                    }
                }.padding(.all)
                
        
            }
            .frame(width: 400, height: 390)
            .background(Color(red: 255/255, green: 255/255, blue: 255/255))
            .clipShape(RoundedRectangle(cornerRadius: 20))
        }
    }
}

struct DetailPageList_Previews: PreviewProvider {
    static var previews: some View {
        DetailPageList()
    }
}
