//
//  AttendenceBackButton.swift
//  menupageui
//
//  Created by Vishal Jain on 19/07/22.
//

import SwiftUI

struct AttendenceBackButton: View {
    struct Photo: Decodable, Hashable {
        var image: String
//        var title:String
//        var desc :String
    }

    var menuIcons = [
        Photo(image: "map"),
        Photo(image: "Settings")
        ]
 
    var body: some View {
        VStack(alignment:.leading){
            HStack( spacing:15){
                Button(action: {
                    print("button pressed")
                }) {
                    Image("back")
                        .resizable()
                        .frame(width: 25, height: 25)
                }
                Text("Attendence")
                    .fontWeight(.bold)
                    .foregroundColor(.black)
                    
                    
            }.frame(width: 144, height: 10)
            .padding(.trailing,250)
            .padding(.all,20)
//            .background(Color(red: 227/255, green: 227/255, blue: 227/255))
//            Spacer()

        }
        
    }
}

struct AttendenceBackButton_Previews: PreviewProvider {
    static var previews: some View {
        AttendenceBackButton()
    }
}
