//
//  LeaveButton.swift
//  menupageui
//
//  Created by Vishal Jain on 20/07/22.
//

import SwiftUI

struct LeaveButton: View {
    var body: some View {
        VStack {
            Spacer()
            Button(action: {
                   print("Rounded Button")
               }, label: {
                   Text("APPLY FOR LEAVE")
                       .bold()
                       .padding(.all,15)
//                       .bold()
                       .frame(width: 340, height: 20)
                       .padding()
                       .background(Color(red: 55/255, green: 202/255, blue: 183/255))
                       .foregroundColor(Color.white)
                       .cornerRadius(13)
           })
        }
    }
}

struct LeaveButton_Previews: PreviewProvider {
    static var previews: some View {
        LeaveButton()
    }
}
