//
//  AttendencePage.swift
//  menupageui
//
//  Created by Vishal Jain on 19/07/22.
//

import SwiftUI

struct AttendencePage: View {
    var body: some View {
        VStack {
            AttendenceBackButton()
            MyTabView()
                .offset( y: -40)
        }.background(Color(red: 227/255, green: 227/255, blue: 227/255))
    }
}

struct AttendencePage_Previews: PreviewProvider {
    static var previews: some View {
        AttendencePage()
    }
}
