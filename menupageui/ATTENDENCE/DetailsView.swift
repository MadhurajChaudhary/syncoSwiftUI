//
//  DetailsView.swift
//  menupageui
//
//  Created by Vishal Jain on 20/07/22.
//

import SwiftUI

struct DetailsView: View {
    private let columns : [GridItem] = [
        GridItem(.flexible(minimum: 50)),
        GridItem(.flexible(minimum: 50))
        
    ]
    
    struct Photo: Decodable, Hashable {
//        var image: String
        var title:String
        var desc :String
    }

    var menuIcons = [
        Photo( title: "Casual", desc: "0/0"),
        Photo( title: "Paid", desc: "0/0"),
        Photo( title: "Sick", desc: "0/0"),
        Photo( title: "Other", desc: "0/0"),

    ]
  
    var body: some View {
        ScrollView(){
 // vstack1
        VStack ( spacing: 20){
            
            VStack (alignment: .leading ,spacing: 0){
                Text("Your Leaves")
                    .frame(width: 340, height: 25, alignment: .leading)
                    .font(.title)
                    
            }.padding(.leading,0)
            
            
                VStack( spacing: 10){
                LazyVGrid(columns : columns){
                    ForEach(menuIcons, id: \.self){ menuIcons in
                        
                        VStack(alignment: .leading, spacing: 9){
                            Text(menuIcons.title).foregroundColor(.blue)
                                .font(.system(size: 29, weight: .light))
                                .frame(width: 100, height: 27, alignment: .center)
                                
                            Text(menuIcons.desc)
                                .font(.title2)
                                .frame(width: 100, height: 20, alignment: .center)
                                .foregroundColor(.black)
                        }
//
                        .frame(maxWidth: 200, maxHeight: 200)
                                .aspectRatio(2, contentMode: .fill)
                                .overlay(RoundedRectangle(cornerRadius: 18).stroke(Color.gray, lineWidth: 0.3))
                    }
                    
                  }
                    
                }
            .padding(.trailing, 20)
            .padding(.leading,20)
            
            
            // put here code deatail page list 
            
            DetailPageList()
              .offset( y: 15)
        }
        .offset( y: 50)
        }.frame(width: .infinity, height: 800)
    }
}

struct DetailsView_Previews: PreviewProvider {
    static var previews: some View {
        DetailsView()
    }
}
