//
//  MarkingPage.swift
//  menupageui
//
//  Created by Vishal Jain on 22/07/22.
//

import SwiftUI

struct MarkingPage: View {
    private let columns : [GridItem] = [
        GridItem(.fixed(200)),
        GridItem(.fixed(200))
        
    ]
    
    struct Photo: Decodable, Hashable {
        var image: String
        var title:String
        var desc :String
    }

    var menuIcons = [
        Photo(image: "map", title: "Map", desc: "Navigate to Map"),
        Photo(image: "geotag", title: "Geoag", desc: "View Schedule Routeh")
    ]
  
    var body: some View {


            VStack {
                ZStack {
                        RoundedRectangle(cornerRadius: 15)
                            .frame(width: 400, height: 100)
                        .foregroundColor(.white)
                    
                    VStack(alignment: .leading, spacing: 10){
                        VStack ( spacing: 10){
                            Text("22 July 2022")
                                .foregroundColor(Color(red: 250/255, green: 157/255, blue: 71/255))
                                .font(.title)
                            
                        }
                        VStack {
                            Text("Today's date")
                                .fontWeight(.bold)
                        }.frame( alignment: .leading)
                        
                    }.padding()
                    .frame(width: 329, height: 100, alignment: .leading)
                }
                
                HStack( spacing: 25){
                    ZStack {
                        RoundedRectangle(cornerRadius: 15)
                            .frame(width: 180, height: 200)
                            .foregroundColor(Color(red: 255/255, green: 255/255, blue: 255/255))
                        VStack (alignment: .leading){
                            
                            VStack {
                                Text("IN TIME")
                                    .fontWeight(.bold)
                            }.padding(.top,5)
                            .padding(.leading,25)
                            
                            
                            VStack {
                                Image("mark_in")
                                    .resizable()
                                    .frame(width: 70, height: 70)
                                .padding(.top)
                            }.padding(.leading,28)
                            
                            
                            
                            VStack {
                                Button(action: {
                                       print("Rounded Button")
                                   }, label: {
                                       Text("MARK IN")
                                           .lineLimit(2)
                                           .frame(width:110, height: 5)
                                           .padding()
                                           .background(Color(red: 55/255, green: 202/255, blue: 183/255))
                                           .foregroundColor(Color.white)
                                           .cornerRadius(5)
                               })
                            }.padding(.top,20)
                            
                            
                        }.padding(.leading,2)
                        
                    }
                    
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 15)
                            .frame(width: 191, height: 200)
                        .foregroundColor(Color(red: 255/255, green: 255/255, blue: 255/255))
                        VStack (alignment: .leading){
                            
                            VStack {
                                Text("Out Time")
                                    .fontWeight(.bold)
                            }.padding(.top,5)
                            .padding(.leading,25)
                            
                            
                            VStack {
                                Image("mark_out")
                                    .resizable()
                                    .frame(width: 70, height: 70)
                                .padding(.top)
                            }.padding(.leading,28)
                            
                            
                            VStack {
                                Button(action: {
                                       print("Rounded Button")
                                   }, label: {
                                       Text("Mark out")
                                           .bold()
                                           .frame(width:110, height: 5)
                                           .padding()
                                           .background(Color(red: 55/255, green: 202/255, blue: 183/255))
                                           .foregroundColor(Color.white)
                                           .cornerRadius(5)
                               })
                            }.padding(.top,20)
                            
                            
                        }.padding(.leading,2)
                        
                    }
                }
                .padding(.top)
                
            }.frame(width: 400, height:350)
            .background(Color(red: 227/255, green: 227/255, blue: 227/255).opacity(0.5))

    }
}

struct MarkingPage_Previews: PreviewProvider {
    static var previews: some View {
        MarkingPage()
    }
}
