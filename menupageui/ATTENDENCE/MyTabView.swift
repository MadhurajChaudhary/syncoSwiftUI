//
//  MyTabView.swift
//  menupageui
//
//  Created by Vishal Jain on 20/07/22.
//

import SwiftUI

struct MyTabView: View {
    var body: some View {
        
        Home()
    }
}

struct MyTabView_Previews: PreviewProvider {
    static var previews: some View {
        MyTabView()
    
    }
}

struct Home : View {
    
    @State var index = 0
    
    var body: some View {
        VStack{
//            HStack {
//                Text("")
//                    .font(.title)
//                    .foregroundColor(Color.blue)
//                    .fontWeight(.bold)
//                Spacer(minLength: 0)
//            }
//            .padding(.horizontal)
            
            // tab view
            
            HStack( spacing: 0) {
                Text(" Markng")
                    .foregroundColor(self.index == 0 ? .white : Color.black.opacity(0.7))
                    .fontWeight(.bold)
                    .padding(.vertical,10)
                    .padding(.horizontal,30)
                    .background(Color(red: 54/255, green: 201/255, blue: 181/255).opacity(self.index == 0 ? 1 : 0))
                    .clipShape(Rectangle()).cornerRadius(10)
                    .onTapGesture {
                        
                        withAnimation(.default){
                            self.index = 0
                        }
                       
                    }
                Spacer(minLength: 0)
                
                Text(" Leaves")
                    .foregroundColor(self.index == 1 ? .white : Color.black.opacity(0.7))
                    .fontWeight(.bold)
                    .padding(.vertical,10)
                    .padding(.horizontal,30)
                    .background(Color(red: 54/255, green: 201/255, blue: 181/255).opacity(self.index == 1 ? 1 : 0))
                    .clipShape(Rectangle()).cornerRadius(10)
                    .onTapGesture {
                        
                        withAnimation(.default){
                            self.index = 1
                        }
                    }
                Spacer(minLength: 0)
                
                Text(" Details")
                    .foregroundColor(self.index == 2 ? .white : Color.black.opacity(0.7))
                    .fontWeight(.bold)
                    .padding(.vertical,10)
                    .padding(.horizontal,30)
                    .background(Color(red: 54/255, green: 201/255, blue: 181/255).opacity(self.index == 2 ? 1 : 0))
                    .clipShape(Rectangle()).cornerRadius(10)
                    .onTapGesture {
                        
                        withAnimation(.default){
                            self.index = 2
                        }
                    }
            }
            .background(Color(red:255/255 , green: 255/255, blue: 255/255))
            .clipShape(Rectangle()).cornerRadius(12).frame( height: 30)
            .padding(.horizontal)
            .padding(.top,25)
            
            // DashBoard Grid
            
      // tab view with swipe gsture
            
            TabView (selection: self.$index){
                //week data

                
                MarkingPage()
                    .offset( y: -170)
               .tag(0)
                LeaveButton() .tag(1)
                VStack{
                    DetailsView()
                }
                .tag(2)
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            
            
            Spacer(minLength: 0)
        }
        .padding(.top)
    }
}







