//
//  TrakingList.swift
//  menupageui
//
//  Created by Vishal Jain on 13/07/22.
//

import SwiftUI

struct TrakingList: View {
    struct Photo: Decodable, Hashable {
        var image: String
        var title:String
        var desc :String
    }

    var menuIcons = [
        Photo(image: "map", title: "Map", desc: "Navigate to Map"),
        Photo(image: "Settings", title: "Settings", desc: "View App Settings"),
        Photo(image: "attendance", title: "Attendance", desc: "See your attendance"),
        Photo(image: "roster", title: "Roster", desc: "View Your Daily Task"),
        Photo(image: "single_order", title: "Single order", desc: "View ongoing Singal Orders"),
        Photo(image: "multi_order", title: "Multi Order", desc: "View ongoing Singal Orders"),
        Photo(image: "order_history", title: "Order History", desc: "View Order History"),
        Photo(image: "scheduler", title: " Route scheduler", desc: "View Schedule Route"),
        Photo(image: "cash", title: "Deposit Cash", desc: "View Schedule Route"),
        Photo(image: "live", title: "Live Stream", desc: "View Schedule Routeh"),
        Photo(image: "distance", title: "TraveL Distance", desc: "View Schedule Routeh"),
        Photo(image: "geotag", title: "Geoag", desc: "View Schedule Routeh"),
        Photo(image: "navigate", title: "", desc: ""),
        Photo(image: "info", title: "", desc: ""),
        Photo(image: "location (1)", title: "", desc: "Pick Up | ETA 18MIN")
    ]
  

    var body: some View {
        VStack( spacing: 6){
            HStack( spacing: 10){
                Text("Accepted 15min ago")
                Spacer()
                Button(action: {
                    print("button pressed")
                }) {
                    Image("navigate")
                        .resizable()
                        .frame(width: 30, height: 30)
                }
                
                Button(action: {
                    print("button pressed")
                }) {
                    Image("info")
                        .resizable()
                        .frame(width: 30, height: 30)
                }
//                Image("navigate")
//                .resizable()
//                .frame(width: 30, height: 30)
//                Image("info")
//                .resizable()
//                .frame(width: 30, height: 30)
            }
            .padding(.top,8)
            .padding(.leading,20)
            .padding(.trailing,20)
            
            
            TrackingList0()
            
            
            TrakingList1()
                .offset(y: 65)
//                .padding(.bottom, 130)
            
            TrackingCustomer()
                .offset(y: 118)
            
            TrackingButtons()
                .offset(y: 143)
            
                
         
            Spacer()
        
        }
      .frame(width: 428, height: 700)
      .background(Color(red: 249/255, green: 249/255, blue: 249/255))
//      .background(Color(red: 249/255, green: 249/255, blue: 2/255))
       
   
    }
}

struct TrakingList_Previews: PreviewProvider {
    static var previews: some View {
        TrakingList()
    }
}
