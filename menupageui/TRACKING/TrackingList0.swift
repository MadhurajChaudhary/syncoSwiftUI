//
//  TrackingList0.swift
//  menupageui
//
//  Created by Vishal Jain on 14/07/22.
//

import SwiftUI

struct TrackingList0: View {
    var body: some View {
        HStack( spacing:97){
            VStack(alignment: .leading,spacing: 5){
                Text("ORD NO.A213FD")
                Text("REF #ORD")
            }
            VStack(alignment: .trailing, spacing: 5) {
                Text("PAYMENT PAID")
                Text("INR 100.00")
            }
        }
        
        .background(RoundedRectangle(cornerRadius: 15).fill(Color.white).frame(width: 400, height: 70))
            .padding(.leading,20)
            .padding(.trailing,20)
            .offset(y: 15)
    }
}

struct TrackingList0_Previews: PreviewProvider {
    static var previews: some View {
        TrackingList0()
    }
}
