//
//  TrackMapView.swift
//  menupageui
//
//  Created by Vishal Jain on 15/07/22.
//

import SwiftUI

struct TrackMapView: View {
    var body: some View {
        VStack{
            MapView()
                .ignoresSafeArea()
                .offset( y: 4)
            TrakingList()
                .offset( y: -20)
//
                
        }
//        .background(Color(red: 249/255, green: 249/255, blue: 249/255))
    }
}

struct TrackMapView_Previews: PreviewProvider {
    static var previews: some View {
        TrackMapView()
    }
}
