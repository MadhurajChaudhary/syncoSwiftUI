//
//  TrackingButtons.swift
//  menupageui
//
//  Created by Vishal Jain on 15/07/22.
//

import SwiftUI

struct TrackingButtons: View {
    var body: some View {
        VStack ( spacing: 11){
            Button(action: {
                   print("Rounded Button")
               }, label: {
                   Text("CANCLE")
                       .bold()
                       .frame(width: 360, height: 13)
                       .padding()
                       .background(Color(red: 238/255, green: 83/255, blue: 89/255))
                       .foregroundColor(Color.white)
                       .cornerRadius(13)
           })
            
            Button(action: {
                   print("Rounded Button")
               }, label: {
                   Text("REACHED MERCHANT")
                       .bold()
                       .frame(width: 360, height: 13)
                       .padding()
                       .background(Color(red: 55/255, green: 202/255, blue: 183/255))
                       .foregroundColor(Color.white)
                       .cornerRadius(13)
           })
            
            Button(action: {
                   print("Rounded Button")
               }, label: {
                   Text("UNDELIVERED")
                       .bold()
                       .frame(width: 360, height: 13)
                       .padding()
                       .background(Color(red: 55/255, green: 202/255, blue: 183/255))
                       .foregroundColor(Color.white)
                       .cornerRadius(13)
           })
        }
    }
}

struct TrackingButtons_Previews: PreviewProvider {
    static var previews: some View {
        TrackingButtons()
    }
}
