//
//  TrakingList1.swift
//  menupageui
//
//  Created by Vishal Jain on 14/07/22.
//

import SwiftUI

struct TrakingList1: View {
    var body: some View {
        VStack( spacing: 10){
            HStack(){
                Image("location (1)").resizable().frame(width: 25, height: 25 ).padding(.bottom,30)
                VStack(alignment: .leading, spacing: 0) {
                  Text("E9400 C Block Mart Road,New Delhi,New Delhi,110053")
                        .bold()
                        .lineLimit(3)
                    
                    Text("Pick UP | ETA 18mins")
                        .foregroundColor(.blue)
                        
                }.padding(.leading,0)

            }
            
            HStack(){
                Image("location_2").resizable().frame(width: 25, height: 25).padding(.bottom,30)
                VStack(alignment: .leading, spacing: 6) {
                  Text("E94 B Block Nehru Vihar,New Delhi,New Delhi,110053")
                        .bold()
                        .lineLimit(3)
                    
                    Text("Lorem| ETA 18mins")
                        .foregroundColor(.blue)
                        
                }.padding(.leading,0)

            }
            
        }
        .background(RoundedRectangle(cornerRadius: 15).fill(Color.white).frame(width: 400, height: 200))
        .padding(.leading,20)
        .padding(.trailing,20)
           
       
    }
}

struct TrakingList1_Previews: PreviewProvider {
    static var previews: some View {
        TrakingList1()
    }
}
