//
//  Ractangalmage.swift
//  menupageui
//
//  Created by Vishal Jain on 14/07/22.
//

import SwiftUI

@available(iOS 15.0, *)
struct Ractangalmage: View {
    
    struct Photo: Decodable, Hashable {
        var image: String
    }
    var menuIcons = [
        Photo(image: "map"),
        Photo(image: "Settings"),
        Photo(image: "attendance"),
        Photo(image: "roster"),
        Photo(image: "single_order")
        
        ]
    
    @available(iOS 15.0, *)
    var body: some View {
        
        
        if #available(iOS 15.0, *) {
            Image("")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 60, height: 60)
                .clipShape(Rectangle())
                .overlay {
                    Rectangle().stroke(.gray, lineWidth: 4).background(.gray).cornerRadius(22)
                }
        } else {
            // Fallback on earlier versions
        }
    }
}

struct Ractangalmage_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            Ractangalmage()
        } else {
            // Fallback on earlier versions
        }
    }
}
