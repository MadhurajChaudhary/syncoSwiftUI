//
//  TrackingCustomer.swift
//  menupageui
//
//  Created by Vishal Jain on 14/07/22.
//

import SwiftUI

struct TrackingCustomer: View {
    var body: some View {
        HStack( spacing: 20){
            if #available(iOS 15.0, *) {
                Ractangalmage().padding(.leading,20)
            } else {
                // Fallback on earlier versions
            }
            
            VStack(alignment: .leading, spacing: 1){
                Text("Customer")
                    .foregroundColor(.black)
                Text("ASHISH")
                    .foregroundColor(.black)
            }
            Spacer()
            Button(action: {
                print("button pressed")
            }) {
                Image("dialer")
                    .resizable()
                    .frame(width: 60, height: 60)
                    .padding(.trailing,20)
            }
//            Image("dialer")
//                .resizable()
//                .frame(width: 60, height: 60).padding(.trailing,20)
            
        }
        .background(RoundedRectangle(cornerRadius: 15).fill(Color.white).frame(width: 400, height: 90))
            .padding(.leading,20)
            .padding(.trailing,20)
//            .offset(y: 15)
           
    }
       
}

struct TrackingCustomer_Previews: PreviewProvider {
    static var previews: some View {
        TrackingCustomer()
    }
}
