//
//  HomePage.swift
//  menupageui
//
//  Created by Vishal Jain on 29/07/22.
//

import SwiftUI

struct HomePage: View {
    @State var currentDate : Date = Date()
    var body: some View {
        ScrollView(.vertical,showsIndicators: false){
            VStack(spacing: 20){

                
                // Custom date picker.........
                
                CustomDatePicker(currentDate: $currentDate)
            }
        }
    }
    
}

struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage()
    }
}
