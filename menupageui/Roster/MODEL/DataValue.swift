//
//  DataValue.swift
//  menupageui
//
//  Created by Vishal Jain on 29/07/22.
//

import Foundation

// Data value Model...

struct DataValue : Identifiable{
    var id = UUID().uuidString
    var day : Int
    var date : Date
    
}

//CustomWatchPicker_Previews
//CustomWatchPicker
