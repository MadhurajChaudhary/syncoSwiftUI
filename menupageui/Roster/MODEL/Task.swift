//
//  Task.swift
//  menupageui
//
//  Created by Vishal Jain on 08/08/22.
//

import Foundation

// Task Model and Simple Task ....

//  array of task
struct Task : Identifiable{
    var id = UUID().uuidString
    var title: String
    var time : Date = Date()
    
}

// total task meta view...
struct TaskMateDate : Identifiable{
    
    var id = UUID().uuidString
    var task : [Task]
    var taskDate : Date
}

// sample data for testing...
func getSampleDate(offset: Int)-> Date{
    let calender = Calendar.current
    let date = calender.date(byAdding: .day, value: offset, to: Date())
    return date ?? Date()
}

// Sample Task
var tasks: [TaskMateDate] = [
 
    TaskMateDate(task: [
        
        
        
        Task(title: "Talk to iJustine"),
        Task(title: "iPhone 13 Great Desing Change"),
        Task(title: "Nothing Much Workkout!!!")
    ], taskDate: getSampleDate(offset: 1)),
 TaskMateDate(task: [
    Task(title: "Talk to jean ")
 ],taskDate: getSampleDate(offset: -3)),
    TaskMateDate(task: [
        Task(title: "Meeting with Tim cook")
    ],taskDate: getSampleDate(offset: -8)),
    
    TaskMateDate(task: [
        Task(title: "Nothing much workout")
    ],taskDate: getSampleDate(offset: -22)),

    TaskMateDate(task: [
        Task(title: "Meeting with Tim cook")
    ],taskDate: getSampleDate(offset: 15)),

    TaskMateDate(task: [
        Task(title: "Kavsoft APp Updates")
    ],taskDate: getSampleDate(offset: -20)),

]
