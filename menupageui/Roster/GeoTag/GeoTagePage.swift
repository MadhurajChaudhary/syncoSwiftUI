//
//  GeoTagePage.swift
//  menupageui
//
//  Created by Vishal Jain on 17/08/22.
//

import SwiftUI

struct GeoTagePage: View {
    var body: some View {
        
        VStack {
            
            HStack ( spacing:20){
                Button(action: {}) {
                        Image(systemName: "chevron.left")
                        .foregroundColor(.black)
                        .scaleEffect(0.9)
                            .font(Font.title.weight(.medium))
                }
                Text("GeoTag").font(.title2)
                
            }.padding(.horizontal,90)
            Spacer()
        }.offset(x: -140, y: 0)
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity
        ).background(Color.white)
        
      
    }
}

struct GeoTagePage_Previews: PreviewProvider {
    static var previews: some View {
        GeoTagePage()
            

    }
}
