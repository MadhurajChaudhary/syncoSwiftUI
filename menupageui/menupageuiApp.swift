//
//  menupageuiApp.swift
//  menupageui
//
//  Created by Vishal Jain on 22/06/22.
//

import SwiftUI

@main
struct menupageuiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
