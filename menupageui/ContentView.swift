//
//  ContentView.swift
//  menupageui
//
//  Created by Vishal Jain on 22/06/22.
//

import SwiftUI

struct ContentView: View {
    @State private var showFavoritesOnly = true
    var body: some View {
        VStack{
            ProfileView()
            collectionView()
//            BasicBottomSheet()
        }.background(Color(red: 249/255, green: 249/255, blue: 249/255))

        }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        
    }
}
