//
//  HistoryPage.swift
//  menupageui
//
//  Created by Vishal Jain on 25/07/22.
//

import SwiftUI

struct HistoryPage: View {
    var body: some View {
        FirstSection()
    }
}

struct HistoryPage_Previews: PreviewProvider {
    static var previews: some View {
        HistoryPage()
    }
}
struct FirstSection : View {
    
    var body: some View{
        VStack{
            HStack( spacing: 25){
                Button(action: {
                    
                }, label: {
                    Image("back")
                        .resizable()
                        .frame(width: 25, height: 25)
                })
                
                Text("History")
                    .fontWeight(.semibold)
                    .font(.title)
                    .foregroundColor(.black)
                
                Spacer()
                
                Button(action: {
                    
                }, label: {
                    Image("info")
                        .resizable()
                        .frame(width: 25, height: 25)
                })
                
               
            }.padding()
            SecondView()
                .offset(x: 0, y: 10)
        }
        
       
    }
}

struct SecondView : View {
    var gridItems : [GridItem] = [
        GridItem(.fixed(150), spacing: 50),
        GridItem(.fixed(150), spacing: 50)
        
    ]
    
    struct Photo: Decodable, Hashable {
        var title:String
        var desc :String
    }

    var menuIcons = [
        Photo(title: "0", desc: "Order count"),
        Photo( title: "INR0.00", desc: "Earnings"),
        Photo( title: "0", desc: "Delivered count"),
        Photo( title: "0", desc: "Cancelld count")
    ]
  
    
    
    var body: some View {
       
            ScrollView(.vertical) {
                LazyVGrid(columns: gridItems, alignment: .center, spacing: 20) {
                    ForEach(menuIcons, id: \.self) { menuIcons in
                        VStack (alignment: .leading, spacing: 5){
                            Text(menuIcons.title)
                                .font(.largeTitle)
                                .foregroundColor(Color(red: 250/255, green: 157/255, blue: 71/255))
                            Text(menuIcons.desc)
                                .foregroundColor(Color.secondary)
                                .fontWeight(.semibold)
                        }  .frame(width: 150, height: 100)
                            .padding(12)
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow(radius: 1)
                        
                    }
                }
            }.background(Color(red: 227/255, green: 227/255, blue: 227/255).opacity(0.7))
       }
  }

// create custom calender in swiftui


