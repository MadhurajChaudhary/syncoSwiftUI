//
//  Imageview.swift
//  menupageui
//
//  Created by Vishal Jain on 22/06/22.
//

import SwiftUI

struct Imageview: View {
    struct Photo: Decodable, Hashable {
        var image: String
    }
    var menuIcons = [
        Photo(image: "map"),
        Photo(image: "Settings"),
        Photo(image: "attendance"),
        Photo(image: "roster"),
        Photo(image: "single_order")
        
        ]
  
    var body: some View {
       
        if #available(iOS 15.0, *) {
            Image("map")
                .clipShape(Circle())
                .overlay {
                    Circle().fill(.gray).frame(width: 70, height:70)
                }.padding(.all,12)
        } else {
            // Fallback on earlier versions
        }
       
                  
    }
}

struct Imageview_Previews: PreviewProvider {
    static var previews: some View {
        Imageview()
    }
}
