//
//  ProfileView.swift
//  menupageui
//
//  Created by Vishal Jain on 14/07/22.
//

import SwiftUI

struct ProfileView: View {
    struct Photo: Decodable, Hashable {
        var image: String
       
    }

    var menuIcons = [
        Photo(image: "on (1)"),
        Photo(image: "off (1)")
       
    ]
  
    var body: some View {
        HStack{
            Imageview()
                .frame(width: 60, height: 60)

            VStack(alignment: .leading, spacing: 2){
                Text("Rahul Sharma")
                    .bold()
                Text("+91 78965453623")
                
                Text("inside Geofence")
                    .foregroundColor(.green)
               
            }.frame(width: 150, height: 100)
                .padding()
            Spacer()
            Button(action: {
                print("button pressed")
            }) {
                Image("on (1)")
            }
            
        }.frame(width: 350, height: 55)
            .padding(12)
            .background(Color.red.opacity(0.01))
            .cornerRadius(15)
            .shadow(radius: 0.08)
//        .padding(.leading,20)
//            .padding(.trailing,20)
//
//        .frame(width: 424, height: 100)
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
