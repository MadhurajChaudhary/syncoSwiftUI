//
//  BasicBottomSheet.swift
//  menupageui
//
//  Created by Vishal Jain on 12/07/22.
//

import SwiftUI


struct BasicBottomSheet: View {
    struct Photo: Decodable, Hashable {
        var image: String
        var title:String
        var desc :String
    }
    
    
    var menuIcons = [
        Photo(image: "map", title: "Map", desc: "Navigate to Map"),
        Photo(image: "Settings", title: "Settings", desc: "View App Settings"),
        Photo(image: "attendance", title: "Attendance", desc: "See your attendance"),
        Photo(image: "roster", title: "Roster", desc: "View Your Daily Task"),
        Photo(image: "single_order", title: "Single order", desc: "View ongoing Singal Orders"),
        Photo(image: "multi_order", title: "Multi Order", desc: "View ongoing Singal Orders"),
        Photo(image: "order_history", title: "Order History", desc: "View Order History"),
        Photo(image: "scheduler", title: " Route scheduler", desc: "View Schedule Route"),
        Photo(image: "cash", title: "Deposit Cash", desc: "View Schedule Route"),
        Photo(image: "live", title: "Live Stream", desc: "View Schedule Routeh"),
        Photo(image: "distance", title: "TraveL Distance", desc: "View Schedule Routeh"),
        Photo(image: "geotag", title: "Geoag", desc: "View Schedule Routeh"),
        Photo(image: "restart_service", title: "Restart service", desc: "Restart App service"),
        Photo(image: "sync_data", title: "Sync Data", desc: "Sync Data with server"),
        Photo(image: "X (1)", title: "", desc: ""),
//        Photo(image: "", title: "",desc: "")
    ]
  
    
    
    var body: some View {
        VStack(alignment: .leading, spacing: 1){
            HStack(){
                Text("Setting")
                    .font(.title)
                Spacer()
                
                Button(action: {
                    print("button pressed")
                }) {
                    Image("X (1)")
                        .resizable()
                        .frame(width: 15, height: 15)
                }
                
            }.padding(.all,25)
      
                HStack{
                    Image("restart_service").frame(width: 70, height: 70)
                    VStack(alignment: .leading, spacing: 2){
                        Text("Restart service")
                            .font(.title2)
                        Text("Restart App service")
                            .font (.caption)
                    }
                }
            
            HStack{
                Image("sync_data").frame(width: 70, height: 70)
                VStack(alignment: .leading, spacing: 2){
                    Text("Sync Data")
                        .font(.title2)
                    Text("Sync Data with server")
                        .font (.caption)
                }
            }
            HStack{
                Image("faq").frame(width: 70, height: 70)
                VStack(alignment: .leading, spacing: 2){
                    Text("FAQ")
                        .font(.title2)
                    Text("Frequently Asked Questions")
                        .font (.caption)
                }
            }
            
            HStack{
                Image("chat").frame(width: 70, height: 70)
                VStack(alignment: .leading, spacing: 2){
                    Text("Chat")
                        .font(.title2)
                    Text("Start a Chat")
                        .font (.caption)
                }
            }
            
            HStack{
                Image("term_condition").frame(width: 70, height: 70)
                VStack(alignment: .leading, spacing: 2){
                    Text("Terms And Condition")
                        .font(.title2)
                    Text("Read Terms And Condition")
                        .font (.caption)
                }
            }
            HStack{
                Image("device_info").frame(width: 70, height: 70)
                VStack(alignment: .leading, spacing: 2){
                    Text("Device info")
                        .font(.title2)
                    Text("View Device info")
                        .font (.caption)
                   
                }
            }
            
    
        }
        .frame(width: .infinity, height: 550)
        .background(Color.gray.opacity(0.09))
        .clipShape(RoundedRectangle(cornerRadius: 50.0))

        
    }
}

struct BasicBottomSheet_Previews: PreviewProvider {
    static var previews: some View {
        BasicBottomSheet()
    }
}
