import SwiftUI

struct collectionView: View {
    
    private let columns : [GridItem] = [
        GridItem(.flexible(minimum: 50)),
        GridItem(.flexible(minimum: 50))
        
    ]
    
    struct Photo: Decodable, Hashable {
        var image: String
        var title:String
        var desc :String
    }

    var menuIcons = [
        Photo(image: "map", title: "Map", desc: "Navigate to Map"),
        Photo(image: "Settings", title: "Settings", desc: "View App Settings"),
        Photo(image: "attendance", title: "Attendance", desc: "See your attendance"),
        Photo(image: "roster", title: "Roster", desc: "View Your Daily Task"),
        Photo(image: "single_order", title: "Single order", desc: "View ongoing Singal Orders"),
        Photo(image: "multi_order", title: "Multi Order", desc: "View ongoing Singal Orders"),
        Photo(image: "order_history", title: "Order History", desc: "View Order History"),
        Photo(image: "scheduler", title: " Route scheduler", desc: "View Schedule Route"),
        Photo(image: "cash", title: "Deposit Cash", desc: "View Schedule Route"),
        Photo(image: "live", title: "Live Stream", desc: "View Schedule Routeh"),
        Photo(image: "distance", title: "TraveL Distance", desc: "View fghj Routeh"),
        Photo(image: "geotag", title: "Geoag", desc: "View Schedule Rcfhfvkvhkfgouteh")
    ]
  
    
    
    var body: some View {
        ScrollView(.vertical,showsIndicators: false){
            VStack( spacing: 30){
            LazyVGrid(columns : columns, spacing: 25){
                ForEach(menuIcons, id: \.self){ menuIcons in
                    NavigationLink(destination:DistanceOnePage()){
                        VStack(alignment: .leading, spacing: 0){
                            Image(menuIcons.image).frame(width: 50, height:50)
                                .background(Color.white)
                                .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing:0))
                            Text(menuIcons.title).font(.title2)
                            Text(menuIcons.desc).foregroundColor(.secondary)}.frame(width: 150, height: 100)
                            .padding(12)
                            .background(Color.white)
                            .cornerRadius(8)
                            .shadow(radius: 0.1)
                    
                  
                    
//                    .padding(EdgeInsets(top: 0, leading: 6, bottom: 3, trailing: 6))
//                    .frame(maxWidth: .infinity, maxHeight: 112)
//                            .aspectRatio(1, contentMode: .fill)
//                            .overlay(RoundedRectangle(cornerRadius: 18).stroke(Color.gray, lineWidth: 0.6))
                    }
                
                }
                
            }
//        .padding(.trailing, 20)
//        .padding(.leading,20)
       
        
            
        }
        .padding()
        .background(Color(red: 249/255, green: 249/255, blue: 249/255)).ignoresSafeArea()

    }
}

struct collectionView_Previews: PreviewProvider {
    static var previews: some View {
        collectionView()
          
    }
}
 }
