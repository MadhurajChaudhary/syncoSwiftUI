//
//  DistanceSecondPage.swift
//  menupageui
//
//  Created by Vishal Jain on 18/08/22.
//

import SwiftUI

//struct TodoItem: Identifiable {
//   var id = UUID()
//   var action: String
//}

struct DistanceSecondPage: View {
//    var todoItems: [TodoItem] = [
//       TodoItem(action: "Writing Swift"),
//       TodoItem(action: "Writing SwiftUI")
//    ]
    var body: some View {
        NavigationView {
               NavigationLink(destination: SecondContentView()) {
                   HStack(spacing: 100){
                       Text("22-07-25")
                       Text("0")
                       Image(systemName: "chevron.right" )
                   }
               }.buttonStyle(PlainButtonStyle())
            }
    }
}

struct DistanceSecondPage_Previews: PreviewProvider {
    static var previews: some View {
        DistanceSecondPage()
    }
}

struct SecondContentView: View {
   var body: some View {
      Text("Distance")
   }
}
