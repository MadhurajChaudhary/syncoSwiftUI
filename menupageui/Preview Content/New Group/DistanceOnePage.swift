//
//  DistanceOnePage.swift
//  menupageui
//
//  Created by Vishal Jain on 18/08/22.
//

import SwiftUI

struct DistanceOnePage: View {
    var body: some View {
        NavigationView {
        VStack {
            ZStack{
                RoundedRectangle(cornerRadius: 15)
                    .frame(width: 400, height: 100)
                .foregroundColor(.black)
                
                VStack(alignment: .leading, spacing: 15){
                    Text("00 km").font(.largeTitle)
                        .foregroundColor(Color(red: 55/255, green: 202/255, blue: 183/255))
                    
                    Text("Today's Distance")
                        .foregroundColor(Color(red: 55/255, green: 202/255, blue: 183/255))
                }.padding(.leading,-180)
            }
            
            HStack ( spacing: 40){
                Spacer()
                Text("Date")
                    .foregroundColor(Color(red: 55/255, green: 202/255, blue: 183/255))
                Spacer()
                Text("km")
                    .foregroundColor(Color(red: 55/255, green: 202/255, blue: 183/255))
                Spacer()
            }
            Divider()
           
                   NavigationLink(destination: collectionView()) {
                       HStack(spacing: 100){
                           Text("22-07-25")
                           Text("0")
                           Image(systemName: "chevron.right" )
                       }
                   }.buttonStyle(PlainButtonStyle()).navigationTitle("Distance")
                }
        }
        
    }
}

struct DistanceOnePage_Previews: PreviewProvider {
    static var previews: some View {
        DistanceOnePage()
       
    }
}

